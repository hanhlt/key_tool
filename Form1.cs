﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Key_Tool_VN
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DataGridViewTextBoxColumn proID = new DataGridViewTextBoxColumn();
            proID.HeaderText = "ID";
            DataGridViewTextBoxColumn proSN = new DataGridViewTextBoxColumn();
            proSN.HeaderText = "SNCODE";
            DataGridViewTextBoxColumn result = new DataGridViewTextBoxColumn();
            result.HeaderText = "RESULT";

            DataGridViewCheckBoxColumn dgvcCheckBox = new DataGridViewCheckBoxColumn();
            dgvcCheckBox.HeaderText = "SELECT";

            dataGridView1.Columns.Add(dgvcCheckBox);
            dataGridView1.Columns.Add(proSN);
            dataGridView1.Columns.Add(result);
            


            
            for(int i = 10; i < 105; i++)
            {
                dataGridView1.Rows.Add(true, "804-600-5"+i, "Watting");
            }

            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView1.AllowUserToAddRows = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lth_vip();
        }

        public void lth_vip()
        {
            int h = 1;
            int z = dataGridView1.Rows.Count;
            progressBar1.Minimum = 0;
            progressBar1.Maximum = 100;
            button1.Text = "RUNNING";
            button1.BackColor = Color.Yellow;
            button1.ForeColor = Color.White;
            for (int i=0;i<dataGridView1.Rows.Count;i++)
            {
                double k = ((double) h / z)*100;
                label2.Text = Math.Round(k, 2) + "%";
                dataGridView1.Rows[i].Selected = true;
                dataGridView1[0, i].Value = false;
                dataGridView1[2, i].Value = "OK";
                dataGridView1[2, i].Style.ForeColor = Color.AliceBlue;
                label1.Text = h / z + "  ";
                progressBar1.Value = (int)k;
                h++;

                
                if(i!=0)
                {
                    dataGridView1.Rows[i-1].Selected = false;
                }    
                Application.DoEvents();
                Thread.Sleep(100);
            }

            button1.Text = "SUSCESS";
            button1.BackColor = Color.SkyBlue;
            button1.ForeColor = Color.White;
        }
    }
}
